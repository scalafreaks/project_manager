import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../@shared/auth.guard';
import { BoardsComponent } from './boards/boards.component';
import { CalendarComponent } from './calendar/calendar.component';
import { GanttComponent } from './gantt/gantt.component';
import { PagesComponent } from './pages.component';
import { SettingsComponent } from './settings/settings.component';
import { WikiComponent } from './wiki/wiki.component';

const routes: Routes = [
  { path: '', loadChildren: () => import('./grid/grid.module').then(m => m.GridModule) },
  {
    path: 'project/:id', component: PagesComponent, children: [
      { path: '', redirectTo: 'boards', pathMatch: 'full' },
      { path: 'calendar', component: CalendarComponent },
      { path: 'boards', component: BoardsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'gantt', component: GanttComponent },
      { path: 'wiki', component: WikiComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
