import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ComponentsModule } from '../@shared/components/components.module';
import { BoardsComponent } from './boards/boards.component';
import { WikiComponent } from './wiki/wiki.component';
import { GanttComponent } from './gantt/gantt.component';
import { CalendarComponent } from './calendar/calendar.component';
import { SettingsComponent } from './settings/settings.component';


import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import { FullCalendarModule } from '@fullcalendar/angular';

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);

const PAGES = [
  CalendarComponent,
  SettingsComponent,
  BoardsComponent,
  GanttComponent,
  WikiComponent
]

@NgModule({
  declarations: [PagesComponent, ...PAGES],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FullCalendarModule,
    ComponentsModule
  ]
})
export class PagesModule { }
