import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Project } from 'src/app/@shared/models/project';
import { GridService } from 'src/app/@shared/services/grid.service';

@Component({
  selector: 'apm-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {

  public projects$: Observable<Array<Project>>;
  constructor(private http: GridService) { }

  ngOnInit(): void {
    this.projects$ = this.http.getProjects();
  }

}
