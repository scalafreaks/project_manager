import { createAction } from "@ngrx/store";

export const login = createAction('[Login Component] Login');
export const logout = createAction('[Logout Component] Logout');
