import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class GridService {

  constructor(private http: HttpClient) { }

  public getProjects(): Observable<Array<Project>> {
    return this.http.get<Array<Project>>('http://127.0.0.1:8080/projects');
  }
}
