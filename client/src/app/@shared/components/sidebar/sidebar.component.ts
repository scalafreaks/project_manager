import { Component, OnInit } from '@angular/core';
import { SidebarItem } from '../../models/sidebar_item';

@Component({
  selector: 'apm-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public projectName: string = 'Scala';
  public userName: string = 'Ola';

  public ITEM_LIST: Array<SidebarItem> = [
    { id: 1, label: 'HOME', link: 'home' } as SidebarItem,
    { id: 2, label: 'BOARDS', link: 'boards' } as SidebarItem,
    { id: 3, label: 'GANTT', link: 'gantt' } as SidebarItem,
    { id: 4, label: 'CALENDAR', link: 'calendar' } as SidebarItem,
    { id: 5, label: 'COMUNICATOR', link: 'comunicator' } as SidebarItem,
    { id: 6, label: 'WIKI', link: 'wiki' } as SidebarItem
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
