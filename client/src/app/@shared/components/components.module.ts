import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';


const IE_MODULES = [

];

const IE_DECLARATIONS = [
  SidebarComponent
];

@NgModule({
  declarations: [
    ...IE_DECLARATIONS
  ],
  imports: [
    CommonModule,
    RouterModule,
    ...IE_MODULES
  ],
  exports: [
    ...IE_MODULES,
    ...IE_DECLARATIONS
  ]
})
export class ComponentsModule { }
