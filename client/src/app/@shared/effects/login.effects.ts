import { Injectable } from "@angular/core";
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from "rxjs";
import { LoginService } from "../services/login.service";

@Injectable()
export class LoginEffects {
  login$ = createEffect(() => this.actions$.pipe(
    ofType('[Login Component] Login'),
    mergeMap(() => this.loginService.login({})
      .pipe(
        map(login => ({ type: '[Login API] Login success', payload: login })),
        catchError(() => EMPTY)
      ))
  ));

  constructor(
    private actions$: Actions,
    private loginService: LoginService
  ) { }
}
