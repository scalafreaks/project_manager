import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { login } from '../@shared/actions/login.actions';

@Component({
  selector: 'apm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login$: Observable<any> = this.store.select(state => state.login);

  constructor(private store: Store<{ login: any }>, private router: Router) { }

  ngOnInit(): void { }

  public signIn() {
    this.store.dispatch(login())
    localStorage.setItem('auth', 'true');
    this.router.navigate(['/']);
  }

}

