from aiohttp import web
import json

from aiohttp.web_response import Response


async def get_users(request):
    pool = request.app['pool']
    async with pool.acquire() as connection:
        result = await connection.fetch("SELECT json_agg(jsonb_build_object('id', id, 'email', email)) FROM app_user;")
        return web.Response(text=result[0][0], content_type='application/json')


async def get_userid(request):
    try:
        get_userid = request.query['id']
        print("Getting user with id: ", get_userid)
        pool = request.app['pool']
        async with pool.acquire() as connection:
            check = await connection.fetch(f"SELECT json_agg(jsonb_build_object(id, email)) FROM app_user where id = {get_userid};")
            if str(check) != "[<Record json_agg=None>]":
                result = check
                return web.Response(text=result[0][0], content_type='application/json')
            else:
                response_obj = {'status': 'falied',
                                'message': f'User with id: {get_userid} doesn\'t exist'}
                return web.Response(text=json.dumps(response_obj), status=500)
    except Exception as e:
        response_obj = {'status': 'falied', 'message': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)
