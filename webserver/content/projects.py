from aiohttp import web
import json

# Pobiera wszystkie projekty


async def get_projects(request):
    pool = request.app['pool']
    async with pool.acquire() as connection:
        result = await connection.fetch("SELECT json_agg(jsonb_build_object('id', id, 'name', name)) FROM project;")
        return web.Response(text=result[0][0], content_type='application/json')

# Pobiera z bazy i zwraca projekt o podanym 'id'


async def get_projectid(request):
    try:
        get_projid = request.query['id']
        print("Getting project with id: ", get_projid)
        pool = request.app['pool']
        async with pool.acquire() as connection:
            check = await connection.fetch(f"SELECT json_agg(jsonb_build_object(id, name)) FROM project where id = {get_projid};")
            if str(check) != "[<Record json_agg=None>]":
                result = check
                return web.Response(text=result[0][0], content_type='application/json')
            else:
                response_obj = {'status': 'falied',
                                'message': f'project with id: {get_projid} doesn\'t exist'}
                return web.Response(text=json.dumps(response_obj), status=500)
    except Exception as e:
        response_obj = {'status': 'falied', 'message': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)

# Dodaje projekt o podanej nazwie w 'name' do bazy


async def add_project(request):
    try:
        n_proj = request.query['name']
        print("Creating a new project with name: ", n_proj)
        pool = request.app['pool']
        req_params = await request.post()
        async with pool.acquire() as connection:
            result = await connection.fetch(f"select add_project(jsonb_build_object('name', '{n_proj}'));")

        response_obj = {'status': 'success',
                        'message': 'new project successfully created'}
        return web.Response(text=json.dumps(response_obj), status=200)
    except Exception as e:
        response_obj = {'status': 'falied', 'message': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)

# Usuwa projekt z bazy na podstawie nazwy w 'name' (można zmienić na id bo w sumie nazwy mogą się powtarzać)


async def del_project(request):
    try:
        del_proj = request.query['name']
        print("Delete project with name: ", del_proj)
        pool = request.app['pool']
        async with pool.acquire() as connection:
            check = await connection.fetch(f"SELECT json_agg(jsonb_build_object('name', '{del_proj}')) FROM project where name = '{del_proj}';")
            if str(check) != "[<Record json_agg=None>]":
                result = await connection.fetch(f"select del_project(jsonb_build_object('name', '{del_proj}'));")
                response_obj = {'status': 'success',
                                'message': 'project successfully deleted'}
                return web.Response(text=json.dumps(response_obj), status=200)
            else:
                response_obj = {'status': 'falied',
                                'message': 'project doesn\'t exist'}
                return web.Response(text=json.dumps(response_obj), status=500)
    except Exception as e:
        response_obj = {'status': 'falied', 'message': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)

# Zmienia nazwę projektu: Pobiera nową nazwe dla projektu z 'name' i pobiera 'id' w którym ma zadziałać


async def ren_project(request):
    try:
        ren_proj = request.query['name']
        id_proj = request.query['id']
        print("Rename project with name: ", ren_proj)
        pool = request.app['pool']
        async with pool.acquire() as connection:
            check = await connection.fetch(f"SELECT json_agg(jsonb_build_object('id', '{id_proj}')) FROM project where id = '{id_proj}';")
            if str(check) != "[<Record json_agg=None>]":
                result = await connection.fetch(f"select ren_project(jsonb_build_object('name', '{ren_proj}', 'id', {id_proj}));")
                response_obj = {'status': 'success',
                                'message': 'project successfully renamed'}
                return web.Response(text=json.dumps(response_obj), status=200)
            else:
                response_obj = {'status': 'falied',
                                'message': 'project doesn\'t exist'}
                return web.Response(text=json.dumps(response_obj), status=500)
    except Exception as e:
        response_obj = {'status': 'falied', 'message': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)
