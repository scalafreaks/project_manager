from aiohttp import web
import aiohttp_cors
import asyncpg
import json
import yaml
import ssl

import logging
logging.basicConfig(level=logging.DEBUG)


def load_yaml(path):
    try:
        with open(path, 'r') as f:
            return yaml.safe_load(f)
    except Exception as e:
        print(str(e))
        return None


async def heart_beat(request):
    return web.Response(text='Ok')


async def app_factory():
    app = web.Application(client_max_size=1024**10)
    config = load_yaml('./config/conn.yaml')

    """ Create database connection pool. """
    app['pool'] = await asyncpg.create_pool(
        database=config['db_name'],
        password=config['db_pass'],
        user=config['db_user'],
        port=config['db_port'],
        host=config['db_host'],
        ssl=ssl._create_unverified_context()
    )

    # heart beat func
    app.router.add_route('GET', '/heart_beat', heart_beat)

    # auth endpoints
    # sign in
    # sign up
    # sign out
    # is_authorized

    # projects endpoints
    from content import projects
    # take all projects
    app.router.add_route('GET', '/projects', projects.get_projects)
    # take project by id
    app.router.add_route('GET', '/projectsid', projects.get_projectid)
    # create project
    app.router.add_post('/projects-add', projects.add_project)
    # delete project
    app.router.add_delete('/projects-delete', projects.del_project)
    # rename project
    app.router.add_put('/projects-rename', projects.ren_project)

    # etc...

    # users endpoints
    from content import users
    app.router.add_route('GET', '/users', users.get_users)
    # get user by id
    app.router.add_route('GET', '/users-id', users.get_userid)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)
    return app


sslcontext = ssl.SSLContext(ssl.PROTOCOL_TLS)
try:
    sslcontext.load_cert_chain(
        './keys/cert.pem',
        './keys/privkey.pem'
    )
except:
    sslcontext = None

web.run_app(
    app_factory(),
    ssl_context=sslcontext
)
